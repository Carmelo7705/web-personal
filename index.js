const express = require('express')
const app = express()

const cors = require('cors')
const morgan = require('morgan')
const { apply } = require('./config/config')


/**Middlewares */
app.use(morgan('dev'));
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());


/** Base de datos */
require('./db')

/** Configure header http */
/*app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method"
  );
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
  res.header("Allow", "GET, POST, OPTIONS, PUT, DELETE");
  next();
});*/


/** Routes */
const routes = require('./routes/api');
app.use('/api', routes);

app.listen(apply.PORT, () => {
  console.log('#####################')
  console.log('##### API REST ######')
  console.log('#####################')
  console.log(`### PORT ${apply.PORT}/${apply.API_VERSION} ####`)
});