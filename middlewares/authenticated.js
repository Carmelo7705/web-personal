const jwt = require('jwt-simple');
const moment = require('moment');

const SECRET_KEY = "secretkey"

module.exports = {
    ensureAuth: (req, res, next) => {
        if(!req.headers.authorization) {
            res.status(403).json({ message: "Sin cabecera de autentificación." });
        }

        const token = req.headers.authorization.replace(/['"]+/g, "");

        try {
            let payload = jwt.decode(token, SECRET_KEY);

            if(payload.exp <= moment.unix()) {
                return res.status(404).json({ message: "El token ha expirado." });
            }

        } catch (err) {
            console.log(err);
            res.status(500).json({ message: "Token inválido o vencido." });
        }

        //req.user = payload;
        next();
    },
};