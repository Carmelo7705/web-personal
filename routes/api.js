const express = require('express');
const router = express.Router();

const middleware = require('../middlewares/authenticated');
const multer = require('multer');
const storage = multer.diskStorage({
    destination: (req, res, cb) => {
        cb(null, './uploads/avatar/');
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + '.' + file.originalname.split(".").pop());
    }
});

const upload_avatar = multer({storage: storage});

const { UserController, AuthController } = require('../controllers');

/** Users Routes */
router.post('/signup', UserController.signUp);
router.post('/login', UserController.signIn);

/** Auth routes */
router.post('/refresh-access-token', AuthController.refreshAccessToken);

/** Users */
router.get('/users', [middleware.ensureAuth], UserController.index);
router.post('/users', [middleware.ensureAuth], UserController.createUser);
router.get('/users-active', [middleware.ensureAuth], UserController.getUserActive);
router.put('/upload-image/:id', [middleware.ensureAuth, upload_avatar.single('avatar')], UserController.uploadAvatar);
router.get('/get-avatar-url/:avatarName', UserController.getAvatarUrl);
router.put('/users/:id', [middleware.ensureAuth], UserController.updateUser);
router.put('/users/activeordesactive/:id', [middleware.ensureAuth], UserController.activeOrDesactiveUser);

module.exports = router