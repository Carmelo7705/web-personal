import React, { useState } from 'react'
import { Layout } from 'antd'
import { Route, Switch, Redirect } from 'react-router-dom';

import useAuth from '../hooks/useAuth';

import TopMenu from '../Components/Admin/TopMenu/TopMenu'
import MenuSider from '../Components/Admin/SideMenu/SideMenu'
import AdminSignin from '../pages/Admin/SignIn'
import "./AdminLayout.scss"

export default function AdminLayout(props) {
  const { routes } = props;
  const [menuCollapse, setMenuCollapse] = useState(false)
  const { Header, Content, Footer } = Layout;

  const { user, isLoading } = useAuth();

  if(!user && !isLoading) {
    return (
      <>
        <Route path="/admin/login" component={AdminSignin}></Route>
        <Redirect to="/admin/login"></Redirect>
      </>
    );
  }

  if(user && !isLoading) {
    return (
      <Layout>
        <MenuSider menuCollapse={menuCollapse} />
        <Layout className="admin-layout" style={{ marginLeft: menuCollapse ? '80px' : '200px' }}>
          <Header className="admin-layout__header">
            <TopMenu menuCollapse={menuCollapse} setMenuCollapse={setMenuCollapse} />
          </Header>
          <Content className="admin-layout__content">
            <LoadRoutes routes={routes} />
          </Content>
          <Footer className="admin-layout__footer">
            Carmelo Fallone Rossi
          </Footer>
        </Layout>
      </Layout>
    )
  }

  return null;
}

function LoadRoutes(props) {
  const { routes } = props

  return (
    <Switch>
      {routes.map((route, index) => (
          <Route key={index} path={route.path} exact={route.exact} component={route.component} />
        ))
      }
    </Switch>
  );
}
