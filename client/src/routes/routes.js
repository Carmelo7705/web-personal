/** Layouts */
import LayoutAdmin from '../layouts/AdminLayout'
import LayoutBasic from '../layouts/BasicLayout'

/** Admin Pages */
import AdminHome from '../pages/Admin'
import AdminSignIn from '../pages/Admin/SignIn'
import AdminUsers from '../pages/Admin/Users'

/** Pages */
import Home from '../pages/Home'
import Contact from '../pages/Contact'
import Error404 from '../pages/Error404'

const routes = [
  {
    path: "/admin",
    component: LayoutAdmin,
    exact: false, //No es exact, porq por ej, si es exact, /admin/contact, queremos que igual nos cargue este layout
    routes: [
      {
        path: "/admin",
        component: AdminHome,
        exact: true
      },
      {
        path: "/admin/login",
        component: AdminSignIn,
        exact: true
      },

      {
        path: "/admin/users",
        component: AdminUsers,
        exact: true
      },
      {
        path: "*",
        component: Error404
      }
    ]
  },
  {
    path: "/",
    component: LayoutBasic,
    exact: false, //No es exact, porq por ej, si es exact, /admin/contact, queremos que igual nos cargue este layout
    routes: [
      {
        path: "/",
        component: Home,
        exact: true
      },
      {
        path: "/contact",
        component: Contact,
        exact: true
      },
      {
        path: "*",
        component: Error404
      }
    ]
  }
];

export default routes;