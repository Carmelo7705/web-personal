import React, { useState } from 'react'
import { Form, Checkbox, Input, Button, notification  } from 'antd'
import { UserOutlined, LockOutlined } from '@ant-design/icons'

import { emailValidation } from '../../../utils/formValidation'
import { signUpApi } from '../../../api/auth'
import "./SignupForm.scss"

export default function SignupForm() {
  const [form] = Form.useForm();
  const [inputs, setInputs] = useState(initialSignup())

  const onChange = (e) => {
    setInputs({...inputs, [e.target.name] : e.target.value})
  }

  const resetForm = () => {
    form.resetFields();
  }

  const onSubmit = (e) => {
    //e.preventDefault();
    let val = validation(inputs)

    if(val) {
      signUpApi(inputs).then(response => {
        notification['success']({
          message: "Te has registrado exitosamente."
        });
        resetForm()
      })
      .catch(error => {
        if(error.response.status === 403) {
          notification['error']({
            message: error.response.data.message
          });
          return;
        }
        
        notification['error']({
          message: "Usuario ya registrado."
        });
      });
    }
  }

  const validation = (data) => {

    if(!data.email 
      || !data.password 
      || !data.repeat_password 
      || !data.privacyPolicy) {
      notification['error']({
        message: "Existen datos vacíos en el formulario."
      })
      return false;
    }

    if(!emailValidation(data.email)) {
      notification['error']({
        message: "El email no es válido."
      })
      return false;
    }

    return true;
  }

  return (
    <Form form={form} className="signup-form" onFinish={onSubmit}>
      <Form.Item name="email">
        <Input 
          prefix={<UserOutlined/> }
          type="email" 
          name="email" 
          placeholder="Correo electronico" 
          className="signup-form__input"
          onChange={onChange} />
      </Form.Item>
      <Form.Item name="password" >
        <Input 
          prefix={<LockOutlined/> }
          type="password" 
          name="password" 
          placeholder="Contraseña" 
          className="signup-form__input"
          onChange={onChange} />
      </Form.Item>
      <Form.Item name="repeat_password" >
        <Input 
          prefix={<LockOutlined/> }
          type="password" 
          name="repeat_password" 
          placeholder="Repetir contraseña" 
          className="signup-form__input"
          onChange={onChange} />
      </Form.Item>
      <Form.Item name="privacyPolicy" valuePropName="false">
        <Checkbox
          name="privacyPolicy"
          onChange={(e) => setInputs({...inputs, [e.target.name]: e.target.checked})}>
          Acepto las politicas de privacidad.
        </Checkbox>
      </Form.Item>
      <Form.Item>
        <Button 
          htmlType="submit"
          className="signup-form__form-button">
          Crear cuenta
        </Button>
      </Form.Item>
    </Form>
  )
}

function initialSignup() {
  return {
    email: "",
    password: "",
    repeat_password: "",
    privacyPolicy: false
  };
}
