import React, { useState, useEffect } from 'react';
import { Switch, List, Avatar, Button, notification } from 'antd';
import { EditOutlined, StopOutlined, DeleteOutlined, CheckOutlined } from '@ant-design/icons';
import NoAvatar from '../../../../assets/png/no-avatar.png'

import ModalComponent from '../../../ModalComponent/ModalComponent';
import EditUserForm from '../EditUserForm/EditUserForm';
import CreateUserForm from '../CreateForm'
import { getAvatarApi, activeOrDesactiveUserApi } from '../../../../api/user';
import { getAccessToken } from '../../../../api/auth';

import "./ListUsers.scss";

export default function ListUsers(props) {

	const { usersActive, usersInactive, setReloadUsers } = props; 
	const [viewUsersActive, setViewUsersActive] = useState(true);
	const [isVisible, setIsVisible] = useState(false);
	const [modalTitle, setModalTitle] = useState("");
	const [modalContent, setModalContent] = useState(null);

	const onCreateUser = () => {
		setIsVisible(true);
		setModalTitle("Nuevo usuario");
		setModalContent(
		<CreateUserForm 
			setReloadUsers={setReloadUsers} 
			setIsVisible={setIsVisible} 
		/>);
	}

	return (
		<div className="list-users">
			<div className="list-users__switch">
				<Switch 
					defaultChecked
					onChange={ () => setViewUsersActive(!viewUsersActive) }
				/>
				<span>
					{ viewUsersActive ? "Usuarios activos" : "Usuarios inactivos" }
				</span>
			</div>

			<div className="list-users__btn">
				<Button type="primary" onClick={onCreateUser}>
					Crear usuario
				</Button>
			</div>

			{ 
				viewUsersActive ? 
					<UsersActive 
						usersActive={ usersActive } 
						isVisible={isVisible}
						setIsVisible={setIsVisible} 
						setModalTitle={setModalTitle}
						setModalContent={setModalContent}
						setReloadUsers={setReloadUsers}
					/> : 
					<UsersInactive usersInactive={usersInactive} setReloadUsers={setReloadUsers} /> 
			}

			<ModalComponent 
				title={modalTitle}
				isVisible={isVisible}
				setIsVisible={setIsVisible}
			>
				{modalContent}
			</ModalComponent>

		</div>
	)
}

function UsersActive(props) {
	const { usersActive, isVisible, setIsVisible, setModalTitle, setModalContent, setReloadUsers } = props;

	const editUser = (user) => {
		setIsVisible(!isVisible);
		setModalTitle(`Editar ${user?.name} ${user?.lastname}`);

		setModalContent(
			<EditUserForm 
				user={user} 
				setIsVisible={setIsVisible} 
				setReloadUsers={setReloadUsers} 
			/>);
	}

	const desactiveUser = user => {
		const token = getAccessToken();

		activeOrDesactiveUserApi(token, user._id, false)
		.then(response => {
			notification["success"]({
				message: response.data.message
			});

			setReloadUsers(true);
		})
		.catch(error => {
			notification["error"]({
				message: error.message
			});
		})
	}

	return (
		<List 
			className="users-active"
			itemLayout="horizontal"
			dataSource={ usersActive }
			renderItem={ user => <UserActive user={user} editUser={editUser} desactiveUser={desactiveUser} />}
		/>
	);
}

function UserActive(props) {
	const { user, editUser, desactiveUser } = props;

	const [avatar, setAvatar] = useState(null);

	useEffect(() => {
		if(user.avatar) {
			getAvatarApi(user.avatar).then(response => {
				const url = `data:image/jpeg;charset=utf-8;base64,
					${Buffer.from(response.data, 'binary').toString('base64')}`
				setAvatar(url);
			});

			return;
		}

		setAvatar(null);
	}, [user])

	return (
		
		<List.Item
			actions={
				[
					<>
						<Button 
							type="primary"
							onClick={ () => editUser(user) }
						>
							<EditOutlined />
						</Button>
						<Button
							type="ghost"
							onClick={ () => desactiveUser(user) }
						>
							<StopOutlined />
						</Button>
						<Button
							type="default"
							onClick={ () => console.log('Eliminar usuario') }
						>
							<DeleteOutlined />
						</Button>
					</>
				]
			}
		>
			<List.Item.Meta 
				avatar={<Avatar size={50} src={ avatar ? avatar : NoAvatar }/>}
				title={
					`${user.name ? user.name : "..."} 
					${user.lastname ? user.lastname : "..."}` 
				}
				description={user.email}
			/>
		</List.Item>

	);

}

function UsersInactive(props) {
	const { usersInactive, setReloadUsers } = props;

	const activeUser = user => {
		const token = getAccessToken();

		activeOrDesactiveUserApi(token, user._id, true)
		.then(response => {
			notification["success"]({
				message: response.data.message
			});

			setReloadUsers(true);
		})
		.catch(error => {
			notification["error"]({
				message: error.message
			});
		})
	}

	return (
		<List 
			className="users-inactive"
			itemLayout="horizontal"
			dataSource={ usersInactive }
			renderItem={ user => <UserInactive user={user} activeUser={activeUser}  />}
		/>
	);
}

function UserInactive(props) {
	const { user, activeUser } = props;

	const [avatar, setAvatar] = useState(null);

	useEffect(() => {
		if(user.avatar) {
			getAvatarApi(user.avatar).then(response => {
				const url = `data:image/jpeg;charset=utf-8;base64,
					${Buffer.from(response.data, 'binary').toString('base64')}`;
				setAvatar(url);
			});

			return;
		}

		setAvatar(null);
	}, [user])

	return (
		
		<List.Item
			actions={
				[
					<>
						<Button
							type="primary"
							onClick={ () => activeUser(user) }
						>
							<CheckOutlined />
						</Button>
						<Button
							type="default"
							onClick={ () => console.log('Eliminar usuario') }
						>
							<DeleteOutlined />
						</Button>
					</>
				]
			}
		>
			<List.Item.Meta 
				avatar={<Avatar size={50} src={ avatar ? avatar : NoAvatar }/>}
				title={
					`${user.name ? user.name : "..."} 
					${user.lastname ? user.lastname : "..."}` 
				}
				description={user.email}
			/>
		</List.Item>

	);

}
