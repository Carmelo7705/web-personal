import React, { useState, useCallback, useEffect } from 'react';
import { Avatar, Form, Input, Select, Button, Row, Col, notification } from 'antd';
import { UserOutlined, MailOutlined, LockOutlined } from '@ant-design/icons';
import { useDropzone } from 'react-dropzone';
import NoAvatar from '../../../../assets/png/no-avatar.png';

import { getAvatarApi, uploadAvatarApi, updateUserApi } from '../../../../api/user';
import { getAccessToken } from '../../../../api/auth'

import "./EditUserForm.scss";

export default function EditUserForm(props) {

	const { user, setIsVisible, setReloadUsers } = props;

	const [avatar, setAvatar] = useState(null);
	const [userData, setUserData] = useState({});

	useEffect(() => {
		setUserData({
			name: user.name,
			lastname: user.lastname,
			email: user.email,
			role: user.role,
			avatar: user.avatar
		})
	}, [user])

	useEffect(() => {
		if(user.avatar) {
			getAvatarApi(user.avatar).then(response => {
				const url = `data:image/jpeg;charset=utf-8;base64,
					${Buffer.from(response.data, 'binary').toString('base64')}`
				setAvatar(url);
			});

			return;
		} 

		setAvatar(null);
		
	}, [user])

	useEffect(() => {
		if(avatar?.file) {
			setUserData({ ...userData, avatar: avatar.file });
		}
	}, [avatar])

	const updateUser = () => {		
		const token = getAccessToken();

		let userUpdate = userData;

		if(userUpdate.password || userUpdate.repeatPassword) {
			if(userUpdate.password !== userUpdate.repeatPassword) {
				notification["error"]({
					message: "Las contraseñas deben ser iguales."
				});

				return;
			}

			delete userUpdate.repeatPassword;
		}

		if(!userUpdate.name || !userUpdate.lastname || !userUpdate.email) {
			notification["error"]({
				message: "Nombre, apellidos o email vacíos."
			});

			return;
		}


		if(typeof userUpdate.avatar === "object") {
			uploadAvatarApi(token, userUpdate.avatar, user._id)
			.then(response => {
				userUpdate.avatar = response.data.user;
			});
		}

		delete userUpdate.avatar;

		updateUserApi(token, userUpdate, user._id).then(result => {
			const { data: { message } } = result;
			notification["success"]({
				message: message
			});

			setIsVisible(false);
			setReloadUsers(true);
		})
		
	}
	
	return (
		<div className="edit-user-form">
			<UploadAvatar 
				avatar={avatar}
				setAvatar={setAvatar}
			/>
			<EditForm  
				userData={userData}
				setUserData={setUserData}
				updateUser={updateUser}
			/>
		</div>
	)
}

function UploadAvatar(props) {
	const { avatar, setAvatar } = props;

	const [avatarURL, setAvatarURL] = useState(null);

	useEffect(() => {
		if(avatar) {
			if(avatar.preview) {
				setAvatarURL(avatar.preview);
			} else {
				setAvatarURL(avatar);
			}
		} else {
			setAvatarURL(null);
		}
	}, [avatar])

	const onDrop = useCallback(
		acceptedFiles => {
			const file = acceptedFiles[0];
			setAvatar({ file, preview: URL.createObjectURL(file) });
		}, [setAvatar],
	);

	const { getRootProps, getInputProps, isDragActive } = useDropzone({
		accept: "image/jpeg, image/png",
		noKeyboard: true,
		onDrop
	}); 

	return (
		<div className="upload-avatar" {...getRootProps()}>
			<input {...getInputProps()} />
			{ isDragActive ? (
				<Avatar 
					size={150}
					src={NoAvatar}
				/>
			) : (
				<Avatar size={150} src={ avatarURL ? avatarURL : NoAvatar } />
			)}
		</div>
	);
}
function EditForm(props) {
	const { userData, setUserData, updateUser } = props;

	const { Option } = Select;

	return (
		<Form className="form-edit" onFinish={updateUser}>
			<Row gutter={24}>
				<Col span={12}>
					<Form.Item>
						<Input 
							prefix={<UserOutlined />}
							placeholder="Nombre"
							value={userData.name}
							onChange={ e => setUserData({ ...userData, name: e.target.value}) }
						/>
					</Form.Item>
				</Col>
				<Col span={12}>
					<Form.Item>
						<Input 
							prefix={<UserOutlined />}
							placeholder="Apellidos"
							value={userData.lastname}
							onChange={ e => setUserData({ ...userData, lastname: e.target.value}) }
						/>
					</Form.Item>
				</Col>
			</Row>
			<Row gutter={24}>
				<Col span={12}>
					<Form.Item>
						<Input 
							prefix={<MailOutlined />}
							placeholder="Email"
							value={userData.email}
							onChange={ e => setUserData({ ...userData, email: e.target.value}) }
						/>
					</Form.Item>
				</Col>
				<Col span={12}>
					<Form.Item>
						<Select
							placeholder="Selecciona un rol de usuario"
							onChange={  e => setUserData({ ...userData, role: e })}
							value={userData.role}
						>
							<Option value="admin">Administrador</Option>
							<Option value="editor">Editor</Option>
							<Option value="reviewr">Revisor</Option>
						</Select>
					</Form.Item>
				</Col>
			</Row>
			<Row gutter={24}>
				<Col span={12}>
					<Input 
						prefix={<LockOutlined />}
						type="password"
						placeholder="Contraseña"
						onChange={ e => setUserData({ ...userData, password: e.target.value }) }
					/>
				</Col>
				<Col span={12}>
					<Input 
						prefix={<LockOutlined />}
						type="password"
						placeholder="Repetir Contraseña"
						onChange={ e => setUserData({ ...userData, repeatPassword: e.target.value }) }
					/>
				</Col>
			</Row>

			<Form.Item>
				<Button 
					type="primary"
					htmlType="submit"
					className="btn-submit"
				>
					Actualizar usuario
				</Button>
			</Form.Item>
		</Form>
	);
}