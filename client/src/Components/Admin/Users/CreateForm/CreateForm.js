import React, { useState } from 'react';
import { Form, Input, Col, Row, Select, Button, notification } from 'antd';
import { UserOutlined, MailOutlined, LockOutlined } from '@ant-design/icons';

import { createUserApi } from '../../../../api/user';
import { getAccessToken } from '../../../../api/auth';

import "./CreateForm.scss";

export default function CreateForm(props) {

	const { setReloadUsers, setIsVisible } = props;
	const [form] = Form.useForm();

	const [userData, setUserData] = useState(initialValues());
	userData.role = "admin";

	const resetForm = () => {
		form.resetFields();
	}

	const createUser = () => {
		const token = getAccessToken();

		if(!userData.name || !userData.lastname || !userData.email || !userData.password || !userData.repeatPassword) {
			notification["error"]({
				message: "Presentas campos vacíos en tus formulario."
			});

			return;
		}

		if(userData.password != userData.repeatPassword) {
			notification["error"]({
				message: "las contraseñas no coinciden."
			});

			return;
		}

		delete userData.repeatPassword;

		createUserApi(token, userData)
		.then(response => {
			console.log(response.user);
			notification["success"]({
				message: "Usuario guardado con éxito."
			});
		}).catch(err => {
			console.log(err);
			if(err.message) {
				notification["error"]({
					message: err.message
				});
			}
		})

		setUserData(initialValues());
		setReloadUsers(true);
		resetForm();
		setIsVisible(false);
	}

	const { Option } = Select;

	return (
		<Form form={form} className="form-create" onFinish={createUser}>
			<Row gutter={24}>
				<Col span={12}>
					<Form.Item name="name">
						<Input 
							prefix={<UserOutlined />}
							placeholder="Nombre"
							onChange={ e => setUserData({ ...userData, name: e.target.value}) }
						/>
					</Form.Item>
				</Col>
				<Col span={12}>
					<Form.Item name="lastname">
						<Input 
							prefix={<UserOutlined />}
							placeholder="Apellidos"
							onChange={ e => setUserData({ ...userData, lastname: e.target.value}) }
						/>
					</Form.Item>
				</Col>
			</Row>
			<Row gutter={24}>
				<Col span={12}>
					<Form.Item name="email">
						<Input 
							prefix={<MailOutlined />}
							placeholder="Email"
							onChange={ e => setUserData({ ...userData, email: e.target.value}) }
						/>
					</Form.Item>
				</Col>
				<Col span={12}>
					<Form.Item name="role">
						<Select
							placeholder="Selecciona un rol de usuario"
							onChange={  e => setUserData({ ...userData, role: e })}
							defaultValue={userData.role}
						>
							<Option value="admin">Administrador</Option>
							<Option value="editor">Editor</Option>
							<Option value="reviewr">Revisor</Option>
						</Select>
					</Form.Item>
				</Col>
			</Row>
			<Row gutter={24}>
				<Col span={12}>
					<Form.Item name="password">
						<Input 
							prefix={<LockOutlined />}
							type="password"
							placeholder="Contraseña"
							onChange={ e => setUserData({ ...userData, password: e.target.value }) }
						/>
					</Form.Item>
				</Col>
				<Col span={12}>
					<Form.Item name="repeatPassword">
						<Input 
							prefix={<LockOutlined />}
							type="password"
							placeholder="Repetir Contraseña"
							onChange={ e => setUserData({ ...userData, repeatPassword: e.target.value }) }
						/>
					</Form.Item>
				</Col>
			</Row>

			<Form.Item>
				<Button 
					type="primary"
					htmlType="submit"
					className="btn-submit"
				>
					Crear usuario
				</Button>
			</Form.Item>
		</Form>
	)
}

function initialValues() {
	return {
		name: "",
		lastname: "",
		email: "",
		role: "admin",
		password: ""
	}
}
