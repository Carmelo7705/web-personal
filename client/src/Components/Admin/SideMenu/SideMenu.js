import React from 'react'
import { Layout, Menu } from 'antd'
import { Link, withRouter } from 'react-router-dom'
import { HomeFilled, UserOutlined } from '@ant-design/icons';

import "./SideMenu.scss"

function SideMenu(props) {
  const { menuCollapse, location } = props
  const { Sider } = Layout;

  return (
    <Sider className="side-menu" collapsed={menuCollapse}>
      <Menu theme="dark" mode="inline" defaultSelectedKeys={[location.pathname]}>
        <Menu.Item key="/admin">
          <Link to={"/admin"}>
            <HomeFilled />
            <span className="nav-text">Home</span>
          </Link>
        </Menu.Item>
        <Menu.Item key="/admin/users">
          <Link to={"/admin/users"}>
            <UserOutlined />
            <span className="nav-text">Usuarios</span>
          </Link>
        </Menu.Item>
      </Menu>
    </Sider>
  )
}

export default withRouter(SideMenu);
