import React from 'react'
import { Button } from 'antd'
import { PoweroffOutlined, MenuFoldOutlined, MenuUnfoldOutlined } from '@ant-design/icons';
import Logo from '../../../assets/png/logo.png'
import { logout } from '../../../api/auth';

import "./TopMenu.scss"

export default function TopMenu(props) {
  const { menuCollapse, setMenuCollapse } = props;

  const logoutUser = () => {
    logout();
    window.location.reload();
  }

  return (
    <div className="top-menu">
      <div className="top-menu__left">
        <img className="top-menu__left-logo" src={Logo} alt="cool" />
      
        <Button type="link" onClick={() => setMenuCollapse(!menuCollapse)}>
        { menuCollapse ? <MenuUnfoldOutlined /> : <MenuFoldOutlined /> }
        </Button>
      </div>

      <div className="top-menu__right">
        <Button type="link" onClick={ logoutUser }>
          <PoweroffOutlined />
        </Button>
      </div>
    </div>
  )
}
