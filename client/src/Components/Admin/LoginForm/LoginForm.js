import React, { useState } from 'react';
import { Form, Input, Button, notification, Spin } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons'
import { ACCESS_TOKEN, REFRESH_TOKEN } from '../../../utils/constants'
import { loginNotEmpty } from '../../../utils/formValidation';
import { signinApi } from '../../../api/auth';

import './LoginForm.scss';

export default function LoginForm() {

	const [inputs, setInputs] = useState({
		email: "", 
		password: ""
	});
	const [loader, setLoader] = useState(false)

	const onChangeForm = (ev) => {
		setInputs({ ...inputs, [ev.target.name]: ev.target.value });
	}

	const login = async () => {

		if(!loginNotEmpty(inputs.email, inputs.password)) {
			notification["error"]({
				message: "Campos vacíos o incorrectos."
			});

			return;
		}

		setLoader(true);
		try {
			const { data: { accessToken, refreshToken } } = await signinApi(inputs);
			localStorage.setItem(ACCESS_TOKEN, accessToken);
			localStorage.setItem(REFRESH_TOKEN, refreshToken);

			notification["success"]({
				message: "Login correcto."
			});

			window.location.href = "/admin";

		} catch (error) {
			notification["error"]({
				message: "Login incorrecto."
			});
		} finally {
			setLoader(false);
		}
	}

	return (
		<Form className="login-form" onChange={ onChangeForm } onFinish={ login }>
			<Form.Item>
				<Input 
					prefix={ <UserOutlined style={{ color: "rgba(0,0,0,.25)" }} /> }
					type="email"
					name="email"
					placeholder="Correo electronico"
					className="login-form__input"
				/>
			</Form.Item>

			<Form.Item>
				<Input 
					prefix={ <LockOutlined style={{ color: "rgba(0,0,0,.25)" }} /> }
					type="password"
					name="password"
					placeholder="Contraseña"
					className="login-form__input"
				/>
			</Form.Item>
			<Form.Item>
				<Button htmlType="submit" className="login-form__button" disabled={loader}>
					{ loader ? <Spin /> : "Entrar" }
				</Button>
			</Form.Item>
		</Form>
	)
}
