import React from 'react';
import { Modal } from 'antd'; //Modal as ModalAntd para añadirle alias.

import "./ModalComponent.scss";

export default function ModalComponent(props) {
	const { children, title, isVisible, setIsVisible } = props;

	return (
		<Modal 
			title={title}
			centered
			visible={isVisible}
			onCancel={ () => setIsVisible(!setIsVisible) }
			footer={false}
		>
			{ children }
		</Modal>
	)
}
