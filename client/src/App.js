import React from 'react';

import Routing from './routes/routing'
import AuthProvider from './providers/AuthProvider';
import './App.scss';

function App() {
  return (
    <AuthProvider>
      <Routing />
    </AuthProvider>
  );
}

export default App;
