export function minLengthValidation(data, minLength) {
  const { value } = data

  if(value.length >= minLength) {
    return true;
  }

  return false;

}

export function emailValidation(email) {
  const emailValid = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

  const result = emailValid.test(email)
  if(result) {
    return true;
  }

  return false;

}

export function loginNotEmpty(email, password) {
  if(email == "" || password == "") {
    return false;
  }

  return true;
}