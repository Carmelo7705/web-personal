import api from '../utils/api';
import { ACCESS_TOKEN, REFRESH_TOKEN } from '../utils/constants';
import jwtDecode from 'jwt-decode';

export function signUpApi(data) {
  const params = {
    headers: {
      "Content-Type": "application/json"
    }
  };

  return new Promise((resolve, reject) => {
    api.post('/signup', data, params)
    .then(response => {
      resolve(response)
    })
    .catch(err => {
      reject(err)
    });
  })
}

export function signinApi(data) {
  const params = {
    headers: {
      "Content-Type": "application/json"
    }
  };

  return new Promise((resolve, reject) => {
    api.post('/login', data, params)
      .then(response => {
        resolve(response)
      })
      .catch(err => {
        reject(err)
      });
  });
}

export function getAccessToken() {
  const at = localStorage.getItem(ACCESS_TOKEN);

  if(!at || at === "null") {
    return null;
  }

  return willExpiredToken(at) ? null : at; // Si devuelve true (Que expiró) retorna null, sino el access token
}

export function getRefreshToken() {
  const refreshToken = localStorage.getItem(REFRESH_TOKEN);

  if(!refreshToken || refreshToken === "null") {
    return null;
  }

  return willExpiredToken(refreshToken) ? null : refreshToken;
}

function willExpiredToken(token) {
  const seconds = 60;
  const { exp } = jwtDecode(token);

  const now = (Date.now() + seconds) / 1000;
  return now > exp; // Sí la fecha actual es mayor a la de exp devuelve true si es menor, false.
}

export function refreshAccessToken(refreshToken) {
  const url = '/refresh-access-token';

  const params = {
    headers: {
      "Content-Type": "application/json"
    }
  };

  const data = {
    refreshToken: refreshToken
  };

  api.post(url, data, params)
  .then(response => {
    if(response.status !== 200) {
      logout();
    }

    const { accessToken, refreshToken } = response.data;

    localStorage.setItem(ACCESS_TOKEN, accessToken);
    localStorage.setItem(REFRESH_TOKEN, refreshToken);
  })
  .catch(err => {
    console.log(err);
  });
}

export function logout() {
  localStorage.removeItem(ACCESS_TOKEN);
  localStorage.removeItem(REFRESH_TOKEN);
}