import api from '../utils/api';

export function getUserApi(token) {
    const params = {
        headers: {
            "Content-Type": "application/json",
            Authorization: token
        }
    };

        return new Promise((resolve, reject) => {
            api.get("/users", params)
            .then(response => {
                resolve(response);
            })
            .catch(err => reject(err));
        });
}

export function getUsersActiveApi(token, status) {
    const params = {
        headers: {
            "Content-Type": "application/json",
            Authorization: token
        }
    };

    return new Promise((resolve, reject) => {
        api.get(`/users-active?active=${status}`, params)
        .then(response => {
            resolve(response);
        })
        .catch(err => reject(err));
    });
}

export function uploadAvatarApi(token, avatar, userID) {
    const formData = new FormData();
    formData.append("avatar", avatar, avatar.name);

    const params = {
        headers: {
            Authorization: token,
            'Content-Type': `multipart/form-data`,
        }
    };

    return new Promise((resolve, reject) => {
        api.put(`/upload-image/${userID}`, formData, params)
        .then(response => {
            resolve(response);
        })
        .catch(error => {
            reject(error);
        });
    });
}

export function getAvatarApi(avatarName) {
    return new Promise((resolve, reject) => {
        api.get(`/get-avatar-url/${avatarName}`, { responseType: 'arraybuffer' })
        .then(response => {
            resolve(response);
        })
        .catch(error => {
            reject(error);
        });
    });
}

/*export function getAvatarApi(avatarName) {
    const url = `http://localhost:3977/api/get-avatar-url/${avatarName}`;
  
    return fetch(url)
      .then(response => {
          console.log(response); //Con fetch la url de la imagen llega mucho mejor y más facil. Con axios, llega en formato base64.
        return response.url;
      })
      .catch(err => {
        return err.message;
      });
  }*/

export function createUserApi(token, userData) {
    const params = {
        headers: {
            "Content-Type": "application/json",
            Authorization: token
        }
    };

    return new Promise((resolve, reject) => {
        api.post('/users', userData, params)
        .then(response => {
            resolve(response);
        })
        .catch(error => {
            reject(error)
        })
    });
}

export function updateUserApi(token, user, userID) {
    const params = {
        headers: {
            "Content-Type": "application/json",
            Authorization: token,
        }
    };

    return new Promise((resolve, reject) => {
        api.put(`/users/${userID}`, user, params)
        .then(response => {
            resolve(response);
        })
        .catch(error => {
            reject(error);
        });
    });
}

export function activeOrDesactiveUserApi(token, userID ,status) {
    const params = {
        headers: {
            "Content-Type": "application/json",
            Authorization: token
        }
    };

    const data = { status };

    return new Promise((resolve, reject) => {
        api.put(`/users/activeordesactive/${userID}`,data, params)
        .then(response => {
            resolve(response);
        })
        .catch(error => {
            reject(error);
        });
    });
}