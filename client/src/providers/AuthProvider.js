import React, { useState, useEffect, createContext } from 'react';
import { getAccessToken, refreshAccessToken, getRefreshToken, logout } from '../api/auth';
import jwtDecode from 'jwt-decode';

export const AuthContext = createContext();

export default function AuthProvider(props) {
    const { children } = props;

    const [user, setUser] = useState({
        user: null,
        isLoading: true,
    });

    useEffect(() => {
        checkLoginUser(setUser);
    }, [])

    return <AuthContext.Provider value={user}>{children}</AuthContext.Provider>
}

function checkLoginUser(setUser) {
    const accessToken = getAccessToken();

    if(!accessToken) {
        const refreshToken = getRefreshToken();

        if(!refreshToken) {
            logout();
            setUser({ user: null, isLoading: false });
        } else {
            refreshAccessToken(refreshToken);
            window.location.reload();
        }
    } else {
        setUser({ user: jwtDecode(accessToken), isLoading: false });
    }
}