import React, { useState, useEffect } from 'react';
import { getUsersActiveApi } from '../../../api/user';
import { getAccessToken } from '../../../api/auth';

import ListUsers from '../../../Components/Admin/Users/ListUsers';

import "./Users.scss"

export default function Users() {

	const [usersActive, setUsersActive] = useState([]);
	const [reloadUsers, setReloadUsers] = useState(false);
	const [usersInactive, setUsersInactive] = useState([])
	const token = getAccessToken();

	useEffect(() => {
		
		getUsersActiveApi(token, true).then(response => {
			const { data: { users } } = response;
			setUsersActive(users);
		});

		getUsersActiveApi(token, false).then(response => {
			const { data: { users } } = response;
			setUsersInactive(users);
		});

		setReloadUsers(false);

	}, [token, reloadUsers]);

	return (
		<div className="users">
			<ListUsers 
				usersActive={usersActive}
				usersInactive={usersInactive}
				setReloadUsers={setReloadUsers}
			/>
		</div>
	)
}
