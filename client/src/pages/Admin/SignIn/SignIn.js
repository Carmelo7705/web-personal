import React from 'react'
import { Layout, Tabs } from 'antd'
import { Redirect } from 'react-router-dom'

import SignupForm from '../../../Components/Admin/SignupForm'
import SigninFrom from '../../../Components/Admin/LoginForm'
import Logo from '../../../assets/png/logo.png'
import { getAccessToken } from '../../../api/auth'
import "./Signin.scss"

export default function SignIn() {
  const { Content } = Layout
  const { TabPane } = Tabs

  if(getAccessToken()){
    return<Redirect to="/admin" />
  }

  return (
    <Layout className="signin">
      <Content className="signin__content">
        <h1 className="signin__content-logo">
          <img src={Logo} alt="Mi-logo" />
        </h1>
        <div className="signin__content-tabs">
          <Tabs type="card">
            <TabPane tab={<span>Entrar</span>} key="1">
              <SigninFrom />
            </TabPane>
            <TabPane tab={<span>Nuevo usuario</span>} key="2">
              <SignupForm />
            </TabPane>
          </Tabs>
        </div>
      </Content>
    </Layout>
  )
}
