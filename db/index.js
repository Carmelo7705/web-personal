const mongoose = require('mongoose')
const { database } = require('../config/config')

mongoose.set("useFindAndModify", false)
mongoose.connect(`${database.URI}`, {
  useCreateIndex: true,
  useNewUrlParser: true,
})
  .then(db => console.log('DB conectada correctamente.'))
  .catch(err => {
    console.log('Error al conectar la bd ' + err)
    throw err;
  })