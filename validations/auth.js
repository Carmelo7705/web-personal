const Joi = require('@hapi/joi')

module.exports = {
  register_validate : (data) => {
    const schema = Joi.object({
      email: Joi.string().email().required(),
      password: Joi.string().regex(new RegExp('^[a-zA-Z0-9]{8,32}$')).required(),
      repeat_password: Joi.ref('password')
    });

    const { error } = schema.validate(data);

    if(error) {
      switch(error.details[0].context.key) {
        case 'email':
          return { error: true, message: "Email vacío o inválido." }
        case 'password':
          return { error: true, message: "Password vacío o inválido." }
        case 'repeat_password':
          return { error: true, message: "Las contraseñas no coinciden." }
        default: 
        return { error: true, message: "Error al registrar." }
      } 
    } else {
        return { error: false }
      }
  },

  create_validate : (data) => {
    const schema = Joi.object({
      name: Joi.string().required(),
      lastname: Joi.string().required(),
      email: Joi.string().email().required(),
      role: Joi.string().required(),
      password: Joi.string().regex(new RegExp('^[a-zA-Z0-9]{8,32}$')).required(),
      active: Joi.boolean().required()
    });

    const { error } = schema.validate(data);

    if(error) {
      switch(error.details[0].context.key) {
        case 'name':
          return { error: true, message: "Nombre vacío o inválido." }
        case 'lastname':
          return { error: true, message: "Apellidos vacío o inválido." }
        case 'email':
          return { error: true, message: "Email vacío o inválido." }
        case 'role':
          return { error: true, message: "Rol vacío." }
        case 'password':
          return { error: true, message: "Password vacío o inválido." }
        case 'active':
          return { error: true, message: "Active inválido." }
        default: 
        return { error: true, message: "Error al registrar.", err: error }
      } 
    } else {
        return { error: false }
      }
  },
};