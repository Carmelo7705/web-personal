const bcrypt = require('bcryptjs');
const fs = require('fs')
const path = require('path');
const { User } = require('../models')
const jwt = require('../services/jwt');
const { register_validate, create_validate } = require('../validations/auth')

module.exports = {
  signUp : async (req, res) => {
    const { name, lastname, email, password, repeat_password } = req.body;

    const validate = register_validate({
      email,
      password,
      repeat_password
    });

    if(validate.error) {
      return res.status(403).json({message: validate.message})
    }

    try {
      const pass = await bcrypt.hash(password, 10);

      const data = {
        name,
        lastname,
        email,
        password: pass,
        role: "admin",
        active: false,
      };

      const user = await User.create(data)
      res.status(200).json(user)

    } catch(e) {
      res.status(500).json({
        message: "Ocurrió un error al intentar registrar el usuario.",
        error: e
      });
    }


  },

  signIn: async (req, res) => {
    const { email, password } = req.body;

    const emailMin = email.toLowerCase();

    try {
      const user = await User.findOne({email: emailMin});

      if(!user) {
        return res.status(404).json({ message: 'El email ingresado no existe.' });
      }

      let match = await bcrypt.compare(password, user.password);

      if(!match) {
        return res.status(403).json({ message: 'Tu contraseña es incorrecta.' });
      }

      if(!user.active) {
        res.status(401).json({ code: 401, message: 'Tu usuario no ha sido activado.' });
      } else {
          res.status(200).json(
            { 
              code: 200, 
              accessToken: jwt.createAccessToken(user),
              refreshToken: jwt.refreshToken(user)
            }
          );
      }

    } catch (error) {
      return res.status(500).json({ message: 'Ha ocurrido un error en el servidor. ' + error });
    }
  },

  index: async (req, res) => {
    try {
     const users = await User.find();

     if(!users) {
       return res.status(404).json({ message: 'No se han encontrado usuarios.' });
     }

     return res.status(200).json({ users });


    } catch (error) {
      console.log(error);
    }
  },

  getUserActive: async (req, res) => {
    const { active } = req.query;

    try {
      const users = await User.find({ active });
      
      if(!users) {
        return res.status(404).json({ message: 'No se han encontrado usuarios.' });
      }

      return res.status(200).json({ users });
    } catch (error) {
      console.log(error);
      return res.status(500).json({ error });
    }
  },

  uploadAvatar: async(req, res) => {
    const { id } = req.params; 
    const avatar = req.file;

    try {
      const user = await User.findById({ _id: id });

      if(!user) {
        return res.status(404).json({ message: "No se han encontrado usuarios con este ID." });
      }

      let avatarName;

      if(avatar) {
        let filename = avatar.originalname;

        let extSplit = filename.split(".").pop();
        //let fileExt = extSplit[1];

        avatarName = avatar.filename;

        if(extSplit != "png" && extSplit != "jpg") {
          return res.status(400).json({ message: "No es una extensión de imagen valida." });
        }
      }

      user.avatar = avatarName;
      const resolve = await User.findByIdAndUpdate({ _id: id }, user);

      if(!resolve) {
        return res.status(404).json({ message: "No se ha encontrado el usuario ni actualizado." });
      }

      return res.status(200).json({ user: user.avatar });
      
    } catch (error) {
      console.log(error);
      return res.status(500).json({ message: "Error en el servidor", error });
    }
  },

  getAvatarUrl: async (req, res) => {
    const { avatarName } = req.params;

    const filePath = "uploads/avatar/" + avatarName;

    fs.access(filePath, (err) => {
      if (err) {
        console.error(err)
        return res.status(404).json({ message: 'Imagen no encontrada.' });
      }

      return res.sendFile(path.resolve(filePath));
    });

  },

  createUser: async (req, res) => {
    const { name, lastname, email, role, password } = req.body;

    const data = {
      name,
      lastname,
      email,
      role,
      password,
      active: false
    };

    const validate = create_validate(data);

    if(validate.error) {
      return res.status(403).json({message: validate.message, err: validate.err})
    }
    

    try {
      const pass = await bcrypt.hash(password, 10);
      data.password = pass;

      const user = await User.create(data);

      res.status(200).json({ user });
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: 'Error en el servidor.' });
    }
  },

  updateUser: async(req, res) => {
    let data = req.body;
    const { id } = req.params;

    if(data.password) {
      await bcrypt.hash(data.password, null, null, (err, hash) => {
        if(err) {
          res.status(500).json({ message: "Error al desencriptar la contraseña." });
        } else {
          data.password = hash;
        }
      });
    }

    try {
      const updt = await User.findByIdAndUpdate({ _id: id }, data);

      if(!updt) {
        return res.status(404).json({ message: "Usuario no encontrado." });
      }

      return res.status(200).json({ message: "Usuario actualizado exitosamente." });
    } catch (error) {
      console.log(error);
      return res.status(500).json({ message: `error del servidor: ${error}` });
    }
  },

  activeOrDesactiveUser: async (req, res) => {
    const { status } = req.body;
    const { id } = req.params;

    try {
      const find = await User.findByIdAndUpdate({ _id: id }, { active: status });
      if(!find) {
        return res.json(404).json({ message: 'Usuario no encontrado.' });
      }

      let msg = status ? 'Usuario habilitado exitosamente.' 
        : 'Usuario inhabilitado exitosamente.';
      

      return res.status(200).json({ message: msg, user: find });
      
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: 'Error en el servidor.' });
    }
  }
}