const jwt = require('../services/jwt');
const moment = require('moment');
const { User } = require('../models');

function willExpireToken(token) {
    const { exp } = jwt.decoded(token);

    const currentDate = moment().unix();

    if(currentDate > exp) {
        return true;
    }

    return false;
}

exports.refreshAccessToken = async (req, res) => {
    const { refreshToken } = req.body;
    const isTokenExpired = willExpireToken(refreshToken);

    if(isTokenExpired) {
        res.status(404).json({ message: "El refresh token ha expirado." });
    } else {
        const { id } = jwt.decoded(refreshToken);

        try {
            const userStored = await User.findOne({ _id: id });

            if(!userStored) {
                res.status(404).json({ message: "Usuario no encontrado." });
            }

            res.status(200).json({
                accessToken: jwt.createAccessToken(userStored),
                refreshToken: refreshToken
            });
        } catch (error) {
            console.log(`Error en el servidor ${error}`);
        }
    }
}

/*module.exports = {
    refreshAccessToken
};*/