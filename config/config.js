module.exports = {
  apply: {
    API_VERSION: "v1",
    PORT: process.env.PORT || 3977,
  },
  database: {
    URI: "mongodb://localhost:27017/web-personal",
  }
}